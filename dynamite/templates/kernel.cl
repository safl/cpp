__kernel void some_kernel(__global double* out, __global double* left, __global double* right)
{
	int num  = get_global_id(0);
	out[num] = left[num] + right[num];
}

