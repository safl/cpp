#include <fstream>
#include <iostream>
#include <cstring>
#include <stdlib.h>

#include <dlfcn.h>
#include "libtcc.h"
#include <CL/cl.h>

/**
 *  opencl,     // OpenCL C
 *  gcc, clang, // C++ + ANSI C + Intrinsics + Inline Assembly
 *  tcc,        // ANSI C + Inline Assembly
 *  tcc-lib,    // ANSI C + Inline Assembly
 *
 *  llvm-mc     // machine-code abstraction
 *  llvm-ir     // llvm-intermediate
 *
 *  libjit,     // Pseudo-assembly
 *  asmjit,     // Pseudo-assembly
 *
 *  lightning, 
 *  parrot.     // Parrot VM - Intermediate Representation
 */

// http://stackoverflow.com/questions/13690454/how-to-compile-and-execute-from-memory-directly

//
// Timings for system() to compile a source-file on to shared library,
// load the library and call the compiled function.
//
// compile      = 0.046
// load+call    = 0.00003
//
// Total gcc dynamic compilation overhead: 0.04603 seconds per function call
//
// gcc overload     = 0.04603
// clang overhead   = 0.038
// tcc-process      = 0.0076
// tcc-library      = 0.00013
//
// opencl = ???
//

using namespace std;

/**
 * Read the entire file provided via filename into memory.
 *
 * It is the resposibility of the caller to de-allocate the returned buffer.
 *
 * @return size_t bytes read.
 *
 */
size_t read_file(const char* filename, char** contents)
{
    int size = 0;
    
    ifstream file(filename, ios::in|ios::binary|ios::ate);
    
    if (file.is_open()) {
        size = file.tellg();
        *contents = (char*)malloc(size);
        file.seekg(0, ios::beg);
        file.read(*contents, size);
        file.close();
    }

    return size;
}

                    // How should these varying signatures be handled???
typedef double (*func)(double x, double y);

/**
 * The backend interface.
 *
 * Becomes what it compiles.
 */
class backend {
public:
    virtual int compile(const char* sourcecode, size_t source_len) = 0;
    virtual double execute(double left, double right) = 0;
};

/**
 * compile() forks and executes a system process, the process along with
 * arguments must be provided as argument at time of construction.
 * The process must be able to consume sourcecode via stdin and produce
 * a shared object file.
 * The compiled shared-object is then loaded and made available for execute().
 *
 * Examples:
 *
 *  process tcc("tcc -O2 -march=core2 -fPIC -x c -shared - -o ");
 *  process gcc("gcc -O2 -march=core2 -fPIC -x c -shared - -o ");
 *  process clang("clang -O2 -march=core2 -fPIC -x c -shared - -o ");
 *
 */
class process: backend {
public:
    process(const char* process_str) : handle(NULL), process_str(process_str) {}
    
    int compile(const char* sourcecode, size_t source_len)
    {
        if (handle) {
            dlclose(handle);
            handle = NULL;
        }

        int fd;                                 // Handle for tmp-object-file
        FILE *p;                                // Handle for process
        char lib_fn[]   = "kernels/kernel_XXXXXX";
        char cmd[200];                          // Buffer for command-string
        char *error;

        fd = mkstemp(lib_fn);                   // Filename of object-file
        if (-1==fd) {
            cout << "Failed creating lib-tmp-file!" << endl;
            return 0;
        }
        close(fd);                              // Close it immediatly.

        sprintf(cmd, "%s%s", process_str, lib_fn);  // Merge command-string

        p = popen(cmd, "w");                    // Execute it
        if (!p) {
            cout << "Failed executing process!" << endl;
            return 0;
        }
        fwrite(sourcecode, 1, source_len, p);
        fflush(p);
        pclose(p);
                                                // Load the kernel
        handle = dlopen(lib_fn, RTLD_NOW);      // Load library
        if (!handle) {
            cout << "Failed loading library!" << endl;
            return 0;
        }

        dlerror();                              // Clear any existing error
                                                // Load function from lib
        f = (func)dlsym(handle, "some_kernel");
        error = dlerror();
        if (error) {
            cout << "Failed loading function!" << error << endl;
            return 0;
        }

        return 1;
    }

    double execute(double left, double right)
    {
        return f(left, right);
    }

    ~process()
    {
        if (handle) {
            dlclose(handle);
            handle = NULL;
        }
    }

protected:
    void* handle;
    const char* process_str;
    func f;

};

/**
 * Uses tcc-library bindings to compile a program in-memory.
 */
class tccl: public backend {
public:

    int compile(const char* sourcecode, size_t source_len)
    {
        if (s) {            // Remove previous compilation
            tcc_delete(s);
            f = NULL;
        }

        s = tcc_new();                                  // Create TCC state
        if (!s) {
            cout << "Could not create tcc state!" << endl;
            return 0;
        }
        tcc_set_output_type(s, TCC_OUTPUT_MEMORY);      // Mandatory init of void*

        if (tcc_compile_string(s, sourcecode) == -1) {  // Compile sourcecode
            cout << "Failed Compiling function!" << endl;
            return 0;
        }

        if (tcc_relocate(s, TCC_RELOCATE_AUTO) < 0) {   // Relocate
            cout << "Failed relocating!" << endl;
            return 0;
        }

        f = (func)tcc_get_symbol(s, "some_kernel");     // Load the function
        if (!f) {
            cout << "tcc: Failed loading function!" << endl;
            return 0;
        }

        return 1;
    }

    double execute(double left, double right)
    {
        return f(left, right);
    }

    ~tccl()
    {
        tcc_delete(s);
    }

private:
    TCCState *s;
    func f;
};

/**
 *  Uses the OpenCL platform to generate and execute code.
 */
class opencl: public backend {
public:
    opencl() : numPlatforms(0), platforms(NULL), numDevices(0), devices(NULL)
    {
        status = clGetPlatformIDs(0, NULL, &numPlatforms);
        if (status != CL_SUCCESS) {
            cout << "Error: Getting platforms!" << endl;
            // Abort / exit
        }

        if (numPlatforms > 0) {
            cl_platform_id* platforms = (cl_platform_id* )malloc(numPlatforms* sizeof(cl_platform_id));
            status = clGetPlatformIDs(numPlatforms, platforms, NULL);
            platform = platforms[0];
            free(platforms);
        }

        status  = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 0, NULL, &numDevices);	
        devices = (cl_device_id*)malloc(numDevices * sizeof(cl_device_id));
        status  = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, numDevices, devices, NULL);

        context = clCreateContext(NULL, 1, devices,NULL,NULL,NULL);
        queue   = clCreateCommandQueue(context, devices[0], 0, NULL);
    }

    int compile(const char* sourcecode, size_t sourcecode_len)
    {
        size_t sourceSize[] = {sourcecode_len};
        program = clCreateProgramWithSource(context, 1, &sourcecode, sourceSize, NULL);
        status  = clBuildProgram(program, 1,devices,NULL,NULL,NULL);

        return 1;
    }

    double execute(double left, double right)
    {
        size_t size = 1;
        double *in_a    = (double*) malloc(sizeof(double)*size);
        double *in_b    = (double*) malloc(sizeof(double)*size);
        double *output  = (double*) malloc(sizeof(double)*size);

        for(size_t i=0; i<size; i++) {
            in_a[i] = left;
            in_b[i] = right;
        }

        cl_mem in_a_buf = clCreateBuffer(               // Setup kernel
            context,
            CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR,
            size * sizeof(double),
            (void*)in_a,
            NULL
        );
        cl_mem in_b_buf = clCreateBuffer(
            context,
            CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR,
            size * sizeof(double),
            (void*)in_b,
            NULL
        );
        cl_mem out_buf = clCreateBuffer(
            context,
            CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR,
            size * sizeof(double),
            (void*)output,
            NULL
        );
        kernel = clCreateKernel(program, "some_kernel", NULL);

        status = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&out_buf);
        status = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&in_a_buf);
        status = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&in_b_buf);
                                                        
        size_t global_work_size[1] = {size};        // Execute it!
        status = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, global_work_size, NULL, 0, NULL, NULL);
        clFinish(queue);                            // Wait for it to finish...

        status = clReleaseMemObject(in_a_buf);      // Release mem object.
        status = clReleaseMemObject(in_b_buf);      
        status = clReleaseMemObject(out_buf);

        double res = output[0];

        free(in_a);
        free(in_b);
        free(output);

        return res;
    }

    ~opencl()
    {
        status = clReleaseCommandQueue(queue);  // Release command queue.
        status = clReleaseContext(context);     // Release context.
        free(devices);
    }

private:
    cl_uint numPlatforms;       // Number of platforms
    cl_platform_id* platforms;  // List of platforms
    cl_platform_id platform;    // The chosen platform
    cl_int status;

    cl_uint numDevices;         // Number of devices
    cl_device_id*   devices;    // List of devices

    cl_context context;
    cl_command_queue queue;

    cl_program program;
    cl_kernel kernel;
};

int main()
{
    double res;

    /*
    tccl bck = tccl();
    process bck("tcc -O2 -march=core2 -fPIC -x c -shared - -o ");
    process bck("gcc -O2 -march=core2 -fPIC -x c -shared - -o ");
    opencl bck = opencl();
    */
    process bck("clang -O2 -march=core2 -fPIC -x c -shared - -o ");
    
    char* sourcecode = NULL;
    size_t source_len = read_file("templates/kernel.c", &sourcecode);   // Read sourcecode
    if(!source_len) {
        cout << "Failed reading sourcecode!" << endl;
        return -1;
    }

    int k = 0;
    bck.compile(sourcecode, source_len);
    for(int i=0; i<100000; i++,k++) {
        res = bck.execute(2.0, 3.0);
    }

    free(sourcecode);
    std::cout << "RES=" << res << ". " << k <<std::endl;
    return 0;
}
