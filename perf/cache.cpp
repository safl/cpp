#include <valgrind/callgrind.h>
#include <iostream>
#include <cstdlib>
#include <ctime>

#define N (size_t)26214400
#define BUNDLE_SIZE (size_t)1
#define BLOCK_SIZE (size_t)4000
#define ITERATIONS (size_t)100

/* Visualize */
timespec timediff(timespec start, timespec end)
{
    timespec tmp;
    if ((end.tv_nsec - start.tv_nsec)<0) {
        tmp.tv_sec  = end.tv_sec - start.tv_sec-1;
        tmp.tv_nsec = 1000000000+end.tv_nsec - start.tv_nsec;
    } else {
        tmp.tv_sec  = end.tv_sec  - start.tv_sec;
        tmp.tv_nsec = end.tv_nsec - start.tv_nsec;
    }
    return tmp;
}

template<typename T>
void pprint_array( T *a, size_t start, size_t end )
{
    for(size_t n=start; n<=end; n++) {
        std::cout << a[n];
        if (n<end) {
            std::cout << ",";
        }
    }
    std::cout << std::endl;
}

template<typename T>
void pprint_array( T *a, size_t size )
{
    pprint_array( a, 0, size-1);
}

/* Manipulate arrays */
template<typename T>
void fill( T *a, size_t size, T value )
{
    for(size_t n=0; n<size; n++) {
        a[n] = value;
    }
}

template<typename T, typename Tincr>
void fill_incr( T *a, T value, Tincr incr_value, size_t size )
{
    for(size_t n=0; n<size; n++, value+=incr_value) {
        a[n] = value;
    }
}

/**
 * Shuffle the values of an array
 *
 * @param a The array which elements to shuffle.
 * @param size Size of the array.
 */
template<typename T>
void shuffle( T *a, size_t size )
{
    T value;
    size_t index;

    for(size_t n=0; n<size; n+=2) {
        index = rand() % size;
        value = a[n];
        a[n] = a[index];
        a[index] = value;
    }
}

/* Run through them and do something */
template<typename T>
void traverse( T *a, T *b, T *c, size_t start, size_t end )
{
    for(size_t n=start; n<=end; n++) {
        a[n] = b[n] + c[n];
    }
}

template<typename T>
void traverse_four( T *a, T *b, T *c, size_t start, size_t end )
{
    asm("mov %eax, %eax");
    for(size_t n=start; n<=end; n++) {
        a[n] = b[n] + c[n];
    }
    asm("mov %eax, %eax");
    /*
    for(size_t n=start; n<=end; n+=4) {
        a[n] = b[n] + c[n];
        a[n+1] = b[n+1] + c[n+1];
        a[n+2] = b[n+2] + c[n+2];
        a[n+3] = b[n+3] + c[n+3];
    }
    */
}

template<typename T>
void traverse_eight( T *a, T *b, T *c, size_t start, size_t end )
{
    for(size_t n=start; n<=end; n++) {
        a[n] = b[n] + c[n];
    }
}

template<typename T>
void traverse( T *a, T *b, T *c, size_t start, size_t end, size_t* index )
{
    for(size_t n=start; n<=end; n++) {
        a[index[n]] = b[index[n]] + c[index[n]];
    }
}

template <typename T>
void regular( T *a, T *b, T *c, size_t length )
{
    for(size_t i=0; i<BUNDLE_SIZE; i++) {
        traverse(a, b, c, 0, length-1);
    }
}

template <typename T>
void regular( T *a, T *b, T *c, size_t length, size_t* index )
{
    for(size_t i=0; i<BUNDLE_SIZE; i++) {
        traverse(a, b, c, 0, length-1, index);
    }
}

template <typename T>
void blocked( T *a, T *b, T *c, size_t length )
{
    int32_t nelements   = (int32_t)length,
            trav_start  = 0, 
            trav_end    = -1;
    size_t i;

    while(nelements>0) {
        nelements -= BLOCK_SIZE;
        trav_start = trav_end +1;
        if (nelements > 0) {
            trav_end = trav_start+ BLOCK_SIZE-1;
        } else {
            trav_end = trav_start+ BLOCK_SIZE-1 +nelements;
        }

        for(i=0; i<BUNDLE_SIZE; i++) {
            traverse(a, b, c, trav_start, trav_end);
        }
    }
}

template <typename T>
void blocked(T *a, T *b, T *c, size_t length, size_t* index)
{
    int32_t nelements   = (int32_t)length,
            trav_start  = 0, 
            trav_end    = -1;
    size_t i;

    while(nelements>0) {
        nelements -= BLOCK_SIZE;
        trav_start = trav_end +1;
        if (nelements > 0) {
            trav_end = trav_start+ BLOCK_SIZE-1;
        } else {
            trav_end = trav_start+ BLOCK_SIZE-1 +nelements;
        }

        for(i=0; i<BUNDLE_SIZE; i++) {
            traverse(a, b, c, trav_start, trav_end, index);
        }
    }
}

int main()
{
    timespec ts, te;    // Timing

    float* a = (float*)malloc(N*sizeof(float));         // Allocation
    float* b = (float*)malloc(N*sizeof(float));
    float* c = (float*)malloc(N*sizeof(float));
    size_t* index = (size_t*)malloc(N*sizeof(size_t));
    
    if (!(a && b && c && index)) {
        std::cout << "Allocation error, probably out of memory." << std::endl;
    } else {

        fill_incr<size_t, size_t>(index, 0, 1, N);  // Random index
        //shuffle(index, N);
 
        //fill_incr(index, (size_t)0, 1, N);        // Contigous index 0 -> N
        //fill_incr(index, (size_t)N-1, -1, N);     // Reverse index N -> 0

        fill(a, N, (float)1);
        fill(b, N, (float)1);
        fill(c, N, (float)1);

        CALLGRIND_START_INSTRUMENTATION;
        clock_gettime(CLOCK_REALTIME, &ts);
        for(size_t i=0; i<ITERATIONS; i++) {
            traverse_four(a, b, c, 0, N-1); 
            //regular(a, b, c, N);
            //regular( a, b, c, N, index );
            //blocked( a, b, c, N );
            //blocked(a, b, c, N, index );
        }
        
        clock_gettime(CLOCK_REALTIME, &te);

        timespec diff = timediff(ts, te);
        double elapsed = ((double)diff.tv_sec+((double)diff.tv_nsec/1000000000.0));
        std::cout << "WallC: " << elapsed << std::endl;
        std::cout << "FLOps: " << ITERATIONS*N << std::endl;
        std::cout << "FLOPS: " << (ITERATIONS*N) / elapsed << std::endl;
        CALLGRIND_STOP_INSTRUMENTATION;
        CALLGRIND_DUMP_STATS;
    }

    if (a) {            // De-allocate operands
        free(a);
    }
    if (b) {
        free(b);
    }
    if (c) { 
        free(c);
    } 
    if (index) {
        free(index);
    }

    return 0;
}

