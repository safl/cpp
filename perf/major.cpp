#include <iostream>
#include <omp.h>

#define N 1024ul
#define M 1024ul
#define E 80ul

int main()
{
    unsigned long *array = (unsigned long*)malloc(N*M*E*sizeof(int));
    if (array == NULL) {
        std::cout << "Error allocating..." << std::endl;
        return 1;
    } else {
        #pragma omp parallel for
        for( unsigned long m=0; m<M; m++) {
            for( unsigned long n=0; n<N; n++) {
                for( unsigned long e=0; e<E; e++) {
                    array[m+n+e] = 1;
                }
            }
        }
        free(array); 
    }
    return 0;
}
