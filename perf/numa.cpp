#include <iostream>

#define N 1024*1024*100

int main() {

    int iterations = 100;
    uint32_t *array = (uint32_t*)malloc(N*sizeof(uint32_t));

    if (array == NULL) {

        std::cout << "Error allocating..." << std::endl;
        return 1;

    } 

    std::cout << "Allocated " << N << " times " << sizeof(uint32_t);
    for( int i=0; i < iterations; i++) {

        for( uint32_t n=0; n<N; n++) {
            array[n] = 1;
        }

    }
    free(array);

    return 0;

}
