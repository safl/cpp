#include <iostream>
#include <time.h>
#include <cstdlib>
using namespace std;

timespec timediff(timespec start, timespec end)
{
    timespec tmp;
    if ((end.tv_nsec - start.tv_nsec)<0) {
        tmp.tv_sec  = end.tv_sec - start.tv_sec-1;
        tmp.tv_nsec = 1000000000+end.tv_nsec - start.tv_nsec;
    } else {
        tmp.tv_sec  = end.tv_sec  - start.tv_sec;
        tmp.tv_nsec = end.tv_nsec - start.tv_nsec;
    }
    return tmp;
}

float run_sse(float x, float y, size_t iterations)
{
    float a=0,b=0,c=0,d=0,e=0,f=0,g=0,h=0,k=0,l=0,m=0;

    size_t i = 0;
    while (i<iterations) {
        size_t j =0;
        while (j<10000) {
            //asm("mov %eax, %eax");
            a = x*y+x*y+(float)j;
            b = x*y+x*y;
            c = a+b+x+y;
            d = a+b+x+y+c;
            e = a+b+x+y+c;
            f = a+b+x+y+c;
            g = a+b+x+y+c;
            h = a+b+x+y+c;
            k = a+b+x+y+c;
            l = a+b+x+y+c;
            m = a+b+x+y+c;

            j++;
            //asm("mov %eax, %eax");
        }
        i++;
    }

    double out = 0;
    out = a+b+c+d+e+f+g+h+k+l+m;
    return out;
}

#define ITERATIONS (size_t)1000000

int main()
{
    timespec ts, te;    // Timing
    clock_gettime(CLOCK_REALTIME, &ts);
    double res = run_sse(1.0, 2.0, ITERATIONS);
    clock_gettime(CLOCK_REALTIME, &te);

    timespec diff = timediff(ts, te);
    double elapsed = ((double)diff.tv_sec+((double)diff.tv_nsec/1000000000.0));
    std::cout << "Result:" << res << std::endl;
    std::cout << "WallC: " << elapsed << std::endl;
    std::cout << "FLOps: " << ITERATIONS*10000*25 << std::endl;
    std::cout << "FLOPS: " << (ITERATIONS*10000*25) / elapsed << std::endl;
    return 0;
}

