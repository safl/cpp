#include <cstdio>

enum my_type {
    my_int,
    my_float,
    my_double
} types;

void as_int(void *arg, my_type hint)
{
    int the_int;
    float the_float;
    double the_double;

    switch(hint) {
        case my_int:
            the_int = *(int *)arg;
            printf("Cast to int: %d\n", (int)the_int);
            printf("Cast to int: %d\n", (int)(*(int *)arg));
            break;
        case my_float:
            the_float = *(float *)arg;
            printf("Cast to int: %d\n", (int)the_float);
            printf("Cast to int: %d\n", (int)(*(float *)arg));
            break;
        case my_double:
            the_double= *(double *)arg;
            printf("Cast to int: %d\n", (int)the_double);
            printf("Cast to int: %d\n", (int)(*(double *)arg));
            break;
    }
};

void as_float(void *arg, my_type hint)
{
    switch (hint) {
        case my_int:
            printf("Cast to float: %f\n", (float)(*(int *)arg));
            break;
        case my_float:
            printf("Cast to float: %f\n", (float)(*(float *)arg));
            break;
        case my_double:
            printf("Cast to float: %f\n", (float)(*(double *)arg));
            break;
    }
};

void as_double(void * arg, my_type hint)
{
    switch (hint) {
        case my_int:
            printf("Cast to double: %f\n", (double)(*(int *)arg));
            break;
        case my_float:
            printf("Cast to double: %f\n", (double)(*(float *)arg));
            break;
        case my_double:
            printf("Cast to double: %f\n", (double)(*(double *)arg));
            break;
    }
};

int main( void )
{
    void (*fpfunc) (void *,enum  my_type);
    
    int *int_p;
    int the_int = 5;

    float *float_p;
    float the_float = 5.0;

    double *double_p;
    double the_double = 5.0;

    float_p = &the_float;
    int_p = &the_int;
    double_p = &the_double;

    switch(types) {
        case my_int:
            fpfunc = &as_int;
        break;
        case my_float:
            fpfunc = &as_float;
            break;
        case my_double:
            fpfunc = &as_double;
    }

    as_int( int_p, my_int );
    as_int( float_p, my_float  );
    as_int( double_p, my_double  );

    as_float( int_p, my_int  );
    as_float( float_p, my_float  );
    as_float( double_p, my_double );

    as_double( int_p, my_int  );
    as_double( float_p, my_float  );
    as_double( double_p, my_double  ); 

    return 0;
}

