#include <cstdio>

enum {
    CPHVB_ADD,          // ADD $R, $A, $B; Add arguments element-wise.
    CPHVB_SUBTRACT,     // SUB $R, $A, $B; Substract arguments element-wise.
    CPHVB_MULTIPLY,     // MUL $R, $A, $B; Multiply arguments element-wise.
    CPHVB_DIVIDE,       // DIV $R, $A, $B; Divide arguments element-wise.
    CPHVB_LOGADDEXP,    // LOGADDEXP $R, $A, $B; Logarithm of the sum of exponentiations of the inputs.
    CPHVB_LOGADDEXP2,   // LOGADDEXP $R, $A, $B; Logarithm of the sum of exponentiations of the inputs in base-2.
    CPHVB_TRUE_DIVIDE,
    CPHVB_FLOOR_DIVIDE,
    CPHVB_NEGATIVE,
    CPHVB_POWER,
    CPHVB_REMAINDER,
    CPHVB_MOD,
    CPHVB_FMOD,
    CPHVB_ABSOLUTE,
    CPHVB_RINT,
    CPHVB_SIGN,
    CPHVB_CONJ,
    CPHVB_EXP,
    CPHVB_EXP2,
    CPHVB_LOG,
    CPHVB_LOG10,
    CPHVB_EXPM1,
    CPHVB_LOG1P,
    CPHVB_SQRT,
    CPHVB_SQUARE,
    CPHVB_RECIPROCAL,
    CPHVB_ONES_LIKE,
    CPHVB_SIN,
    CPHVB_COS,
    CPHVB_TAN,
    CPHVB_ARCSIN,
    CPHVB_ARCCOS,
    CPHVB_ARCTAN,
    CPHVB_ARCTAN2,
    CPHVB_HYPOT,
    CPHVB_SINH,
    CPHVB_COSH,
    CPHVB_TANH,
    CPHVB_ARCSINH,
    CPHVB_ARCCOSH,
    CPHVB_ARCTANH,
    CPHVB_DEG2RAD,
    CPHVB_RAD2DEG,
    CPHVB_BITWISE_AND,
    CPHVB_BITWISE_OR,
    CPHVB_BITWISE_XOR,
    CPHVB_LOGICAL_NOT,
    CPHVB_LOGICAL_AND,
    CPHVB_LOGICAL_OR,
    CPHVB_LOGICAL_XOR,
    CPHVB_LEFT_SHIFT,
    CPHVB_RIGHT_SHIFT,
    CPHVB_GREATER,
    CPHVB_GREATER_EQUAL,
    CPHVB_LESS,
    CPHVB_LESS_EQUAL,
    CPHVB_NOT_EQUAL,
    CPHVB_EQUAL,
    CPHVB_MAXIMUM,
    CPHVB_MINIMUM,
    CPHVB_ISFINITE,
    CPHVB_ISINF,
    CPHVB_ISNAN,
    CPHVB_SIGNBIT,
    CPHVB_MODF,
    CPHVB_LDEXP,
    CPHVB_FREXP,
    CPHVB_FLOOR,
    CPHVB_CEIL,
    CPHVB_TRUNC,
    CPHVB_LOG2,
    CPHVB_ISREAL,
    CPHVB_ISCOMPLEX,
    CPHVB_RELEASE,  // ==     CPHVB_SYNC + CPHVB_DISCARD
    CPHVB_SYNC,
    CPHVB_DISCARD,
    CPHVB_DESTROY,  // Inform VEM to deallocate an array.
    CPHVB_RANDOM,   // file out with random
    CPHVB_ARANGE,   // out, start, step
    CPHVB_NONE
};

enum {
    CPHVB_BOOL,
    CPHVB_INT8,
    CPHVB_INT16,
    CPHVB_INT32,
    CPHVB_INT64,
    CPHVB_UINT8,
    CPHVB_UINT16,
    CPHVB_UINT32,
    CPHVB_UINT64,
    CPHVB_FLOAT16,
    CPHVB_FLOAT32,
    CPHVB_FLOAT64,
    CPHVB_INDEX,
    CPHVB_UNKNOWN
};

enum typeopf {
    CPHVB_ADD_BOOL = CPHVB_ADD*100+CPHVB_BOOL
};

int main( void )
{
    long int cphvb_opcode, cphvb_type;
    size_t count = 0;

    for(cphvb_opcode=0; cphvb_opcode<CPHVB_NONE; cphvb_opcode++) {
        for(cphvb_type=0; cphvb_type<CPHVB_UNKNOWN; cphvb_type++, count++) {
            printf("[Opc=%ld,    Type=%ld,    P=%ld]\n", cphvb_opcode, cphvb_type, (cphvb_opcode*100) + cphvb_type);

            switch(cphvb_opcode*100+cphvb_type) {
                case CPHVB_ADD_BOOL:
                    printf("ADD BOOLS\n");
                    break;
                default:
                    break;
            }
        }
    }
    printf("Switch size %zu.\n", count); 
    
    return 0;

}

