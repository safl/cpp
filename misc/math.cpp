#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <cassert>
#include <cmath>

void something( float *op1, float *op2, float *op3 )
{
    *op1 = ldexp( (double)*op2, (int)*op3);
}

void hypot( float *op1, float *op2, float *op3)
{
    *op1 = sqrt( pow(*op3, 2) + pow(*op3, 2) );
    //*op1 = sqrt( pow(*op2, 2) + pow(*op3, 2) );
}

template <typename T>
void frexp( T *op1, T *op2, T *op3)
{
    int fraction;
    *op1 = frexp( *op3, &fraction );
    *op3 = (T)fraction;
}

int main()
{
    float left  = 3.4;
    float right = 2.6;
    float res;


    hypot( &res, &left, &right);
    printf("Op1= %f, Op2= %f, Op3= %f\n", res, left, right );


    frexp( &res, &left, &right);
    printf("Op1= %f, Op2= %f, Op3= %f\n", res, left, right );

    return 0;
}
