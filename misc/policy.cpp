#include <cstdio>
#include <iostream>
#include <cstdlib>

template <typename T1, typename T2, typename T3, typename Instr>
inline void iter( T1* op1, T2* op2, T3* op3)
{
    Instr opcode_func;
    opcode_func( op1, op2, op3 );
}

template <typename T1, typename T2, typename T3>
struct add {
    void operator()( T1 *op1, T2 *op2, T3 *op3) {
        *op1 = *op2 + *op3;
    }
};

int main()
{
    int v1=1, v2=2, v3=3;
    int *op1, *op2, *op3;
    op1 = &v1;
    op2 = &v2;
    op3 = &v3;

    std::cout << "[" << *op1 << "," << *op2 << "," << *op3 << "]."<< std::endl;
    iter<int,int,int, add<int,int,int>>( op1 ,op2, op3);
    //iter<int>( op1 ,op2, op3, add_int);
    std::cout << "[" << *op2 << "+" << *op3 << "=" << *op1 << "]."<< std::endl;
    //iter<int, add>(1,2);
    //std::cout << "And the bla bla is: "<< iter<int, add>( 1 , 2 ) << "." << std::endl;

    return 0;
}

