#include <cstdio>
#include <iostream>
#include <cstdlib>

template <typename... T>
int dosomething(T... args)
{
    std::cout << "Multiple!" << std::endl;
    return 1;
}

template <typename T>
int dosomething(T arg)
{
    std::cout << "One Arg" << std::endl;
    return 1;
}

int main()
{
    /*
    int (*fp)();
    fp = &dosomething;
    fp(1,2,3);
    */

    dosomething(1, 2);
    dosomething(1);
    return 0;
}

