#include <iostream>

using namespace std;

int main(void)
{
    // These evaluate to logical truth
    if (1) {
        cout << "if (1); And that is the truth!" << endl;
    }
    if (-1) {
        cout << "if (-1); And that is the truth!" << endl;
    }
    if (2) {
        cout << "if (2); And that is the truth!" << endl;
    }
    if (true) {
        cout << "if (true); And that is the truth!" << endl;
    }

    // These do not...
    if (0) {
        cout << "if (0); And that is the truth!" << endl;
    }
    if (NULL) {
        cout << "if (NULL); And that is the truth!" << endl;
    }
    if (false) {
        cout << "if (false); And that is the truth!" << endl;
    }

    // Unless they are negated
    if (!0) {
        cout << "if (!0); And that is the truth!" << endl;
    }
    if (!NULL) {
        cout << "if (!NULL); And that is the truth!" << endl;
    }
    if (!false) {
        cout << "if (!false); And that is the truth!" << endl;
    }

    return 0;
}

