#include "stdio.h"

// Enums have a numerical value in the order they are defined.
// GOOD = 0, CHARM = 2
enum {
    GOOD,
    LUCK,
    CHARM
};

// Within the scope of the definition.
// HAVE = 0, TONIGHT = 2.
enum {
    HAVE,
    FUN,
    TONIGHT
};

enum SOMETHING {
    KNOWN,
    UNKNOWN,
    VALUED,
    HATED
};

// An equivalent implementation would be using defines
#define FOOD 0
#define ALA 1
#define CARTE 2

int main(void)
{
    int i;
    for(i=0; i<CARTE; i++) {
        printf("Hello world %d\n", i);
    }

    printf("GOOD = %d, CHARM = %d\n", GOOD, CHARM);
    printf("HAVE = %d, TONIGHT = %d\n", HAVE, TONIGHT);
    printf("FOOD = %d, CARTE = %d\n", FOOD, CARTE);

    // Variables can be declared as enum-types.
    // This is however only for readability, it is not type checked.
    enum SOMETHING test;
    test = VALUED;
    test = KNOWN;

    printf("test = %d\n", test);

    return 0;
}

