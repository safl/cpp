// setprecision example
#include <iostream>
#include <iomanip>
using namespace std;

int main ()
{
  double f =3.14159;
  cout << setprecision (0) << f << endl;
  cout << setprecision (5) << f << endl;
  cout << setprecision (9) << f << endl;
  cout << fixed;
  cout << setprecision (5) << f << endl;
  cout << setprecision (9) << f << endl;
  return 0;
}

